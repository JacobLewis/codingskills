﻿using System;
using System.Collections.Generic;
using System.Text;
using CodingSkillsChallenge.Models;
using CsvHelper.Configuration;

namespace CodingSkillsChallenge
{
    public sealed class ProductMap : ClassMap<Product>
    {
        public ProductMap()
        {
            Map(m => m.SKU);
            Map(m => m.Description);
        }
    }
}
