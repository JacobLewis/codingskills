﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CodingSkillsChallenge.Models;
using CsvHelper;

namespace CodingSkillsChallenge
{
    public class MegaMerger
    {
        private const string InputDirectory = "..\\..\\..\\input\\";
        private const string OutputDirectory = "..\\..\\..\\output\\";

        private List<Product> catalogA;
        private List<BarcodeRecord> barcodesA;

        private List<Product> catalogB;
        private List<BarcodeRecord> barcodesB;

        public void Merge()
        {
            InitialLoad();

            List<BarcodeRecord> duplicates = FindDuplicateBarcodes(barcodesA, barcodesB);

            catalogB = RemoveProductsWithDuplicateBarcodes(catalogB, duplicates);

            OutputMergedCatalog(catalogA, catalogB);
        }

        private void InitialLoad()
        {
            catalogA = LoadProductsFromCatalog("catalogA.csv");
            barcodesA = LoadBarcodesFromFile("barcodesA.csv");
            SortCatalogBarcodes(catalogA, barcodesA);

            catalogB = LoadProductsFromCatalog("catalogB.csv");
            barcodesB = LoadBarcodesFromFile("barcodesB.csv");
            SortCatalogBarcodes(catalogB, barcodesB);
        }

        private List<Product> LoadProductsFromCatalog(string catalog)
        {
            // Get the catalog letter
            string source = catalog.Substring(catalog.IndexOf(".") - 1, 1);

            using (var reader = new StreamReader(InputDirectory + catalog))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Context.RegisterClassMap<ProductMap>();

                var products = csv
                    .GetRecords<Product>()
                    .ToList();

                products.ForEach(product => product.Source = source);

                return products;
            }
        }

        private List<BarcodeRecord> LoadBarcodesFromFile(string file)
        {
            using (var reader = new StreamReader(InputDirectory + file))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var barcodes = csv
                    .GetRecords<BarcodeRecord>()
                    .ToList();

                return barcodes;
            }
        }

        private void SortCatalogBarcodes(List<Product> catalog, List<BarcodeRecord> barcodes)
        {
            foreach (BarcodeRecord record in barcodes)
            {
                int index = catalog.FindIndex(product => product.SKU == record.SKU);
                catalog[index].Barcodes.Add(record);
            }
        }

        private List<BarcodeRecord> FindDuplicateBarcodes(List<BarcodeRecord> barcodesA, List<BarcodeRecord> barcodesB)
        {
            HashSet<string> barcodes = new HashSet<string>(barcodesA.Select(b => b.Barcode));

            List<BarcodeRecord> duplicateBarcodes = barcodesB.Where(b => barcodes.Contains(b.Barcode)).ToList();

            return duplicateBarcodes;
        }

        private List<Product> RemoveProductsWithDuplicateBarcodes(List<Product> catalog,
            List<BarcodeRecord> duplicateBarcodes)
        {
            foreach (BarcodeRecord barcode in duplicateBarcodes)
            {
                catalog.RemoveAll(product => product.Barcodes.Contains(barcode));
            }

            return catalog;
        }

        private void OutputMergedCatalog(List<Product> catalogA, List<Product> catalogB)
        {
            using (var writer = new StreamWriter(OutputDirectory + "result_output.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(catalogA);
                csv.WriteRecords(catalogB);
            }
        }
    }
}