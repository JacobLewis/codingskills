﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodingSkillsChallenge.Models
{
    public class Catalog
    {
        public Catalog()
        {
            Products = new List<Product>();
        }

        public string Name { get; set; }
        public List<Product> Products{ get; set; }
    }
}
