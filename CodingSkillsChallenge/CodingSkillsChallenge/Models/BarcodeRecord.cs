﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodingSkillsChallenge.Models
{
    public class BarcodeRecord
    {
        public string SupplierID { get; set; }
        public string SKU { get; set; }
        public string Barcode { get; set; }
    }
}
