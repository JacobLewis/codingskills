﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodingSkillsChallenge.Models
{
    public class Product
    {
        public Product()
        {
            Barcodes = new List<BarcodeRecord>();
        }

        public string SKU { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public List<BarcodeRecord> Barcodes { get; set; }
    }
}
