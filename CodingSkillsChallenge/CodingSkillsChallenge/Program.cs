﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CodingSkillsChallenge.Models;
using CsvHelper;

namespace CodingSkillsChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            MegaMerger merger = new MegaMerger();

            merger.Merge();
        }
    }
}
